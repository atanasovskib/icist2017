% interactcadsample.tex
% v1.03 - April 2017

\documentclass[]{interact}

\usepackage{epstopdf}% To incorporate .eps illustrations using PDFLaTeX, etc.
\usepackage{subfigure}% Support for small, `sub' figures and tables
%\usepackage[nolists,tablesfirst]{endfloat}% To `separate' figures and tables from text if required
\usepackage{listings}

\graphicspath{{figures/}}

\usepackage{natbib}% Citation support using natbib.sty
\bibpunct[, ]{(}{)}{;}{a}{}{,}% Citation support using natbib.sty
\renewcommand\bibfont{\fontsize{10}{12}\selectfont}% Bibliography support using natbib.sty


\newcommand{\circled}[1]{\raisebox{.5pt}{\textcircled{\raisebox{-.9pt} {#1}}}}
\newcommand{\ST}[1]{%
  \ensuremath{\circled{\tiny\strut \raisebox{.6ex}{#1}}}}

\theoremstyle{plain}% Theorem-like structures provided by amsthm.sty
\newtheorem{theorem}{Theorem}[section]
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{proposition}[theorem]{Proposition}

\theoremstyle{definition}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{example}[theorem]{Example}

\theoremstyle{remark}
\newtheorem{remark}{Remark}
\newtheorem{notation}{Notation}

\begin{document}

\title{On defining a model driven architecture \!\!\ST R for an enterprise e-health system}

\author{
\name{
Blagoj Atanasovski\textsuperscript{a},
Miloš\v{s} Bogdanovi\'c\textsuperscript{b},%\thanks{CONTACT Miloš\v{s} Bogdanovi\'c. Email: milos.bogdanovic@elfak.ni.ac.rs}
Goran Velinov\textsuperscript{c},
Leonid Stoimenov\textsuperscript{b},
Aleksandar S. Dimovski\textsuperscript{e},\thanks{CONTACT Aleksandar S. Dimovski. Email: adim@itu.dk}
Dragan Jankovi\'c\textsuperscript{b},
Irena Skrceska\textsuperscript{d},
Margita Kon-Popovska\textsuperscript{c},
Boro Jakimovski\textsuperscript{c}}
\affil{
\textsuperscript{a}Sorsix International, Skopje, Macedonia, blagoj.atanasovski@sorsix.com; \\
\textsuperscript{b}University of Nis, Faculty of Electronic Engineering, Ni\v{s}, Serbia, \{milos.bogdanovic,leonid.stoimenov,dragan.jankovic\}@elfak.ni.ac.rs; \\
\textsuperscript{c}Faculty of Computer Science and Engineering, Ss. Cyril and Methodius University, Skopje, Macedonia, \{goran.velinov,margita.kon-popovska,boro.jakimovski\}@finki.ukim.mk; \\
\textsuperscript{d}Faculty of Informatics, European University, Skopje, Macedonia, irena.skrceska@eurm.edu.mk; \\
\textsuperscript{e}IT University of Copenhagen, Copenhagen, Denmark; adim@itu.dk}
}


\maketitle

\begin{abstract}
The national e-health systems implemented in Serbia as ``MojDoktor'' (MyDoctor) and in Macedonia as ``MojTermin'' (MyAppointment) are based on the same e-health platform. It is an integrated health information system which represents a central electronic system, where all medical health related information about patients, health workers, facilities, documents, procedures is stored and processed on-line. The architecture of the system was designed to allow for process oriented development with agile methodologies. This methodology allowed for fast deployment and adoption, but a change in the architecture to a more formal approach is required to assure its extensibility, soundness, interoperability and standardization. In this paper,
we propose a formalization of the system design and its implementation as a Model Driven Architecture \!\!\ST R \footnote{The OMG group that launched it holds registered trademarks on the term Model Driven Architecture \!\!\ST R and its acronym MDA \!\!\ST R }.
\end{abstract}

\begin{keywords}
Model driven architecture \!\!\ST R, National e-health platform, Integrated information systems, Formal models, and Verification
\end{keywords}


\section{Introduction}

The national e-health platform represents a central electronic system, in which all medical and health related data are stored and processed on-line. The system allows integration with existing and future systems used by hospitals, clinics, the Ministry of Health (MoH), and other public health institutions. It can be classified as a system that integrates several components: an Electronic Health Record (EHR), Electronic Prescriptions, Electronic Referrals, Hospital Stay and Surgeries Information System, Laboratory Information System (LIS), and a Radiology Information System (RIS).

The platform keeps the data in a centralized system. The various health organizations integrated with the national e-health system have autonomous heterogeneous systems who keep data locally, but a part of that data is written in the central database in the process of integration with the national system. The platform was implemented in Macedonia as ``MojTermin'' and in Serbia as ``MyDoctor''. The national system integrates all existing information systems used in public and private health organizations that have an agreement with the Public Health Insurance Fund (PHIF).
Third-party applications and information systems are integrated using a precisely defined API accessible through web services with a HTTPS connection. The access to the web services can be public or protected. The services that are public provide access to public data about the health organizations and specialties, doctors, resources, available time slots, etc. Protected services provide access to patient data, referrals, examinations (inspections by medical professionals), prescription details, etc. Protected services require authentication.
A web application was built that offers all the functionalities provided through the APIs, it is meant to be used by patients and the organizations which use third-party software that doesn't cover all the functionalities of the system. Again, the web site has a publicly accessible portal, and sections that require authentication.

A data-flow diagram of the complete platform is given in Figure \ref{fig_dfd}. The diagram depicts the uni- or bidirectional exchange and flow of certain types of data between the national e-health system in the middle, with various ``stakeholders'' in the domain of public healthcare. The national system integrates with the PHIF to receive information about patient's health insurance. With the Drug Agency (DA) it exchanges data about the registered drugs and their supply. The Institute of Public Health (IPH) receives data from the national system in the form of reports and Business Intelligence (BI). The MoH receives reports and BI also, but it can issue controls to the system. Various health-care providers receive reports and BI from the platform, but also exchange patient medical data and their EHR. Patients receive data about their appointments, available resources, and they can cancel their appointments as well.

From a data collection point of view, the system continuously creates new and updates existing records. In Serbia, the system creates records for over 240,000 prescriptions, 80,000 referrals and 170,000 examinations, each working day, while handling over 4,800,000 requests through the web APIs. In Macedonia, the system creates records for over 70,000 prescriptions and 30,000 referrals, each working day, while handling over 1,000,000 requests through the web APIs.

\cite{velinov2015ehr} recognize that long term strategy goals of a national e-health system should be standardization on a national level, and progress to full interoperability on a European level. The current architecture design enabled an implementation of the processes to create a functional system. This allowed ``MojDoktor'' to be built and functional in 9 months. ``MojTermin'' had the booking module implemented in 6 months, with additional modules being added on a regular interval of a few more months.

A change to a more formal approach is required in order to transform the platform to be interoperable and standardized, and do so in a secure and sound way. The current implementation uses standardized international codes for doctor specialties, diagnosis (ICD10), drug generics (ATC). And the next step is to make the system compliant with EHR standards like HL7 or OpenEHR.

\begin{figure}
\centering
\includegraphics[scale=0.30]{pinga_dfd_diagram_on_conceptual_level.eps}
\caption{The platform conceptual level data flow diagram}
\label{fig_dfd}
\end{figure}

\section{Motivation and State-of-the-art}

In order to design a viable Integrated Health Information System (IHIS), software engineers are facing a variety of problems originating from the high complexity of the health care processes they are attempting to conceptualize. Although a certain level of standardization has been achieved in the past decade \cite{EHR2008, HL72011, ISO2009}, there still seems to be missing an overall standard that makes it possible for electronic health care data to be easily interchanged between health enterprises.
Without the ability to easily interchange data, interoperability remains an unsolved issue for majority of health information system and brings both research and development communities back to a set of questions regarding the architecture of these systems: What methodologies should be used to conceptualize the health care processes? Should health care information system always rely on platform-independent models? What architecture should be used to insure successful deployment of information system in heterogeneous and distributed environments? Which interoperability standards should be adopted to enhance health information exchange? What is the impact of all these issues upon software development process?
In the past decade, Model Driven Architecture \!\!\ST R (MDA \!\!\ST R) has been widely adopted as an framework for development of integrated health information systems. This architecture has been foreseen as a possible solution for a wide range of issues troubling the designers of health information systems. MDA \!\!\ST R characteristics offer a possibility to develop health information system as a loosely coupled group of applications functioning in a heterogeneous and distributed environment. If MDA \!\!\ST R is adopted in early design stage, IHIS development process offers system architects an opportunity to design a truly lawful, portable and interoperable system by supporting the following architectural characteristics \cite{Lopez2009}:

\begin{itemize}
\item system flexibility, scalability and re-usability is insured through component-orientation;
\item architecture is model-driven and separated from the software development process;
\item platform-independent and platform-specific modelling processes are separated (imposed through MDA \!\!\ST R characteristics);
\item reference models and domain models can be specified at meta-level which in turn can be used as foundation for the development of shared terminology and/or ontology.
\end{itemize}

In the context of long-term usability and interoperability of IHIS, there is a significant number of examples adopting MDA \!\!\ST R architecture. As stated in \cite{Blobel2006}, Germany has started a national programme for establishing a health telematics platform which combines card enabled communication with network based interoperability. In order to achieve semantic interoperability, this platform successfully combines model driven architecture \!\!\ST R with variety of different generic components and development approaches such as ISO reference model for open distributed processing (RM-ODP) \cite{RM-ODP}, a unified process based on the Rational Unified Process \cite{RUP}, the HL7 development framework \cite{HL7-DF}, service oriented architecture (SOA) \citeyearpar{SOA2007} , CORBA services \cite{CORBA2012}, advanced EHR architecture approaches \cite{EHR2008}, etc.

Another example of development framework for semantically interoperable IHIS has been provided in \cite{Lopez2009}. The main objective of this research was to enhance existing IHIS architectures to enable semantic interoperability. The authors present an analysis of existing IHIS architectures and provide a harmonization framework based on the usage of The Generic Component Model (GCM). The methodology they propose takes advantage of Service-Oriented Architecture (SOA), Model Driven Architecture \!\!\ST R, ISO 10746 and HL7 Development Framework (HDF). Further, the modelling process is based on the Rational Unified Process (RUP) to insure the flexibility of the development while harmonizing different architectural approaches.
In \cite{Rayhupathi2008} authors explore the potential of the model driven architecture \!\!\ST R in health care information systems development. MDA \!\!\ST R was used as a mean to develop a system capable of tracking patient information. They present the underlying MDA \!\!\ST R structure in the form of UML diagrams accompanied by PIM to PSM transformation rules. These rules have been used to generate the prototype application from the model they also present. This development provided additional insights regarding the development of transformation rules. As output, this research provided design guidelines for using MDA \!\!\ST R in IHIS development and confirmed MDA \!\!\ST R is usable for generating general models and has the potential to overcome lack of open standards, interoperability, portability, scalability, and the high cost of implementation issues.

Another possible application of MDA \!\!\ST R is within the information systems focused on achieving semantic interoperability of electronic health records. As an example, the approach described in \cite{Garde2009} focuses on methods which will insure clinical content is internationally accessible and technically and clinically correct. Although this approach can represent the foundation for safely sharing the information clinicians need, decision support and legacy data migration, in order to make a system semantically interoperable all data should conform to shared models, terminologies and/or ontologies which can be introduced through means of MDA \!\!\ST R. In the same context, the authors in \cite{Schlieter2015} attempt to create a specific MDA \!\!\ST R for the health-care domain. They needed a mechanism that fosters a sustainable tele-health platform in terms of a methodology that provides an efficient way to deploy new artefacts on the platform and ensure these artefacts are compliant to the platform properties. They show how the MDA \!\!\ST R approach can be used in order to build a methodological fundament for a systematic creation of an application system. Their use-case is an application for IT based work-flow support for an interdisciplinary stroke care project.

The work \cite{curcin2014model} uses MDA \!\!\ST R to develop tools for data collection which allow non-informatics experts to model requirements in their local domain. They recognized a problem in the process of implementing local improvement initiatives in healthcare systems, so they developed a Model-Driven framework and implementation that allows local teams in medical organizations to specify the metrics to track their performance during an intervention, together with data points to calculate these metrics. Based on the metric specification the software generates appropriate data collection pages.
In \cite{jones2005modelling} MDA \!\!\ST R augmented with formal validation was used to develop m-health components promising portability, interoperability, platform independence and domain specificity. The authors are developing systems based on inter-communicating devices worn on the body (Body Area Networks) that provide an integrated set of personalised health-related services to the user. This mobile healthcare application feeds captured data into a healthcare provider’s enterprise information system. In their previous work the same authors propose an extension of the model-driven approach where formal methods are used to support the process of modelling and model transformation. The semi-formal modelling with UML is verified  by using tools such as SPIN \cite{Holzmann2003} for model checking and TORX  \cite{Tretmans1999}  for model testing. They present \cite{Bolognesi1995} , \cite{Jones1995} and \cite{Jones-TR1997} as possible formal approaches to the transformation.

In this paper we present a transformation from process oriented design to a MDA \!\!\ST R for our e-health platform. Once the platform becomes fully transformed following MDA \!\!\ST R principles and process described in this paper, we believe that both developers and platform users will have the following benefits:

\begin{enumerate}
\item The complexity of platform will be abstracted through means of MDA \!\!\ST R models. In this way, decisions regarding operational design will be delegated to lower modelling levels.
\item The platform will become ready for eHealth standards implementation. MDA \!\!\ST R itself uses standards at many levels. Therefore, it will be capable of supporting eHealth standards by aligning (mapping) its PIMs with the structure of the components defined by different standards.
\item Interoperability will no longer be an issue due to capability of implementing different standards e.g. due to ability to align (map) abstract models to standard components.
\item Development costs regarding the future improvements of the platform are expected to be decreased due to re-usability of the developed models and alignments (mappings).
\end{enumerate}

\section{Transforming informal work-flow diagrams to MDA \!\!\ST R}
In this section, we first give a short description of the development methodology, followed by definitions of terms that will be used throughout the paper. Then we give an example of an informal work-flow diagram for creating referrals. Next, we create a model for web-services, we formalize the representation of work-flow diagrams, and finally we give a framework for formal verification of the process presented in the diagrams. We take the process of creating a referral for a patient in a designated time-slot as an illustrative example.

\subsection{The development of the national e-health system}

The system was developed and implemented in phases with gradual expansion according to the adapted methodology for iterative and gradual development \cite {larman2003}, in the following way:
\begin{enumerate}
	\item By consulting domain experts, we create work-flow diagrams that record the processes and rules in the health-care.
	\item \label{item_rel_model} With these work-flow diagrams and additional discussion, a relational model is made of:
	\begin{enumerate}
		\item all the entities required to describe the data;
		\item all the relations and constraints between entities.
	\end{enumerate}
	\item We create a central database which is populated with the relational model developed in Step (\ref{item_rel_model}).
	\item A service layer to govern the database operations is created, which implements the semantic checks and constraints that can not be modelled with the relational model.
	\item A web application is created, which represents an interface between users and the service layer.
	\item A web API is created to allow third party applications to communicate with the service layer.
\end{enumerate}

This represents a standard, and has been confirmed as successful when implementing complex information systems before. For presentation purposes in the next subsections the focus is on the module for examinations and referrals. This module creates the possibility to define and approve appointment timeslots for doctors, the creation and update of electronic referrals and examinations, a way to make appointments for surgeries, and to follow the activities of doctors in clinics.

\subsection{Processes, Web Services and Entities}
We can define a \emph{Process} as a series of steps taken by a user of the platform in order to achieve a certain goal. The user can be either a patient, a doctor, a ministry employee etc. The steps a user takes can be either selections or data entry in the application fields, calls to the platform's web services, or recursively another process (sub-process).

The platform's \emph{Web Services} (in the following, \emph{Services}) are stateless procedures that take request arguments and store everything in the central database. Some of the services have prerequisites, certain entities must be present in the database and certain properties of those entities should have specific values in order for the service to be allowed to execute. Each such service can either complete successfully or fail.

An \emph{Entity} is a collection of properties that have semantic meaning. In the platform, as in all EIS, entities are first represented as database tables or table projections, and each entity is a separate class in code. Entity instances are the source of truth in the entire information system. Each entity is uniquely identified by its type and a key (ID).

\subsection{An Informal work-flow diagram}
In the platform, there are multiple types of referrals. Certain types of referrals do not require an appointment to be made, they are created referring to a clinic or laboratory, and the first available doctor receives the patient as soon as they arrive. Regular referrals require an available appointment timeslot, and each timeslot can be assigned to only one referral.


\begin{figure}
\centering
\includegraphics[scale=0.6]{create_exam_workflow.eps}
\caption{The work-flow of examination creation}
\label{fig_workflow_exam_creation}
\end{figure}

The work-flow diagrams are the first step we took in the implementation of the system. They represent an informal representation of processes that the system implements. We consider them informal because complex workflows are hard to model, constraints are not presented and interpretation can vary between readers. Because of this, they are not ideal for documenting health-care processes, and significant amount of non-structured text was required to capture all the rules and constraints.

Figure \ref{fig_workflow_exam_creation} represents the process of referral creation in the ``MojDoktor'' web application. First the doctor must be authenticated, and if the required referral is a laboratory referral or the patient's condition requires immediate intervention the doctor selects the clinic the patient will be referred to and creates a referral with the data from the patient's EHR. If the referral is a regular one, the doctor must find a clinic and a doctor (or resource) with available appointment times, and then create a referral with the selected time-slot and patient's EHR data. The electronic referral is stored in the central database.

The work-flow diagram contains user choices (type of referral, filtering), calls to existing web services (create referral), and documents are created (and stored in the database). The web service for creating a referral with a specific time-slot, has the following signature:
\small
\begin{verbatim}
CreateReferralWithTimeslot(Timeslot, Patient, Resource, Description):Referral
\end{verbatim}
where \emph{CreateReferralWithTimeslot} is the name of the service, \emph{Timeslot}, \emph{Patient}, \emph{Resource} and \emph{Description} are input arguments, and the output is either information (identification) about the examination created or an error. Creating an examination creates new objects in the database and modifies existing ones, returns to the user some info about the newly created examination, or an error, depending on what went wrong.

\subsection{A Formal Model for Describing Web Services}
\begin{figure}
\centering
\includegraphics[scale=0.57]{abstract_service_model.eps}
\caption{A class diagram of the Abstract Service Model}
\label{fig_abstract_service_model}
\end{figure}

Using the service definitions in the codebase of the system, a model was abstracted for defining a service, we call this model \emph{The Abstract Service Model}. The class diagram for this model is given in Figure \ref{fig_abstract_service_model}

Using the Abstract Service Model we can describe in general any web service in the system. By creating instances for the \emph{Service} class and all its fields, a model is built for a specific web service.
The \emph{Service} class contains the name of the service, description of the input arguments required to call the service, description of the possible success output, and possible fail output.
The required arguments to call a service can be separated in two groups, \emph{Existing Entities} and \emph{Literals}. Existing entities refer to objects already present in the central database, so they have an unique ID, and literals are required user input data. The \emph{Fail Output Description} of a service contains a description string, signifying the error that occurred. The \emph{Success Output Description} class has two fields, a list of entity types that the client should expect, and dictionary of which entity types get modified by the execution of the service, with some kind of a CRUD operation, but not returned to the client.

With the \emph{Service Invocation} class, together with the \emph{Service Input} and \emph{Service Output} classes a specific call to the service can be described. The \emph{Service Input} must contain values for entities that truly exist in the database. A validation can be built that asserts the invocation arguments match the requirements of the Service.

The code in Figure \ref{fig_service_instance_json} is a JSON description of a model for the \emph{CreateReferralWithTimeslot} service, the service validates if selected entities (patient, resource, time-slot) really exist, and if the user has the privilege to create a referral for the selected patient to the selected resource/doctor in the given time-slot. When the validation process is satisfied, the time-slot is updated to show which referral it is occupied by, and the new referral is persisted. A projection of the complete referral entity (a Referral Data Transfer Object) is returned to the user.

\begin{figure}
\begin{verbatim}
service = {
  "name" : "CreateReferralWithTimeslot",
  "inputDescription": {
    "existingEntities" : {
      "timeslot": "AppointmentTimeslot",
   	  "patient": "Patient",
      "resource": "Resource"
    },
    "literals": {
      "description" : String
    }
  },
  "failOutput" : {
    "errorDescription" : String
  },
  "output": {
    "toReturnToClient": ["ReferralDTO"],
    "entitiesTouched": {
        "AppointmentTimeslot": "UPDATE",
        "Referral": "CREATE"
    }
  }
}
\end{verbatim}
\caption{A JSON representation of an instance of the \emph{Service} class from the Abstract Service Model}
\label{fig_service_instance_json}
\end{figure}

A specific invocation for the \emph{CreateReferralWithTimeslot} service as described in Figure \ref{fig_serviceinvocation_instance_json} would specify which entities should be used when creating the referral, which patient is referred to which doctor in which appointment timeslot.

\begin{figure}
\begin{verbatim}
serviceInvocation = {
  "serviceName": "CreateReferralWithTimeslot",
  "input": {
    "existingEntities": {
      "timeslot": 35632,
      "patient": 9392,
      "resource": 2762232
    },
    "literals": {
      "description": "Lorem ipsum ..."
    }
  }
}
\end{verbatim}
\caption{A JSON representation of an instance of the \emph{ServiceInvocation} class}
\label{fig_serviceinvocation_instance_json}
\end{figure}

When the service is invoked the \emph{output} field is not set, it is the received response (if the request was processed correctly). An example output from the \emph{CreateReferralWithTimeslot} service is shown in Figure \ref{fig_serviceoutput_instance_json}. The output specifies which entities were created or updated, e.g. the appointment time-slot with id 35632 is not available for further bookings. The service returns the id, description and/or other properties of the newly created referral, which can be used in subsequent calls to other services.

\begin{figure}
\begin{verbatim}
serviceOutput = {
  "returnedEntities":{
    "ReferralDTO": {
      "id": 29341,
      "description": "Lorem ipsum ...",
      ...
    }
  },
  "entitiesTouched": [
    {
      "entityType": "Referral",
      "crudOperation": "CREATE",
      "entityId": 29341
    },
    {
      "entityType": "AppointmentTimeslot",
      "crudOperation": "UPDATE",
      "entityId": 35632
    }
  ]
}
\end{verbatim}
\caption{A JSON representation of an instance of the \emph{ServiceOutput} class}
\label{fig_serviceoutput_instance_json}
\end{figure}

\subsection{A Formal Model for describing Work-Flow Diagrams}
Once formal models for the web services are created, the work-flow diagrams that were present can be updated. In the work-flow diagrams, action steps can be replaced with calls to Services. Some action steps require calls to multiple different Services, so an action step in the work-flow diagram would can be either another complete process. Continuing our example, in the work-flow diagram in Figure \ref{fig_workflow_exam_creation} the ``create examination'' action would be a Service call, but the actions to find an available time-slot and resource/doctor would be a separate process in which the user would receive a list of resources in which clinics are available first, then select an available time-slot.

The Abstract Service Model allows the input and the output arguments of each service to be described in a structured way. This creates an opportunity to formally check whether the flow of a process is correct, and if all possible outcomes from a service are covered. To facilitate this an Abstract Process Model should be described. An \emph{Abstract Process Model} represents a formal description of a work-flow diagram.

\begin{figure}
\centering
\includegraphics[scale=0.6]{abstract_process_model.eps}
\caption{Abstract model for executables}
\label{fig_abstract_process_model}
\end{figure}

Because one executable step of the process can be either a complete sub-process, application logic or a call to a service, we create an interface describing an executable, which can be any of those three. A process has a directed graph describing its flow, and each node in the graph can be an Executable. Two nodes can be connected if and only if the input of the second node, or part of the input is covered by the output of the first node. Figure \ref{fig_abstract_process_model} is a model for the relationships between the different types of executables.

\begin{figure}[t!]
\begin{verbatim}
    public abstract class Executable {
        protected String name;
        protected object inputDescription;
        protected object outputDescription;

        public abstract String Name { get; set; }
        public abstract object InputDescription { get; set; }
        public abstract object OutputDescription { get; set; }
    }

    public class Process: Executable {
        public Directed_Graph Flow { get; set; }

        public Process(): base() { Flow = new Directed_Graph(); }
        public override String Name {
            get { return name; }
            set { name = value;}    }
        public override object InputDescription {
            get { return inputDescription; }
            set { inputDescription = value; }   }
        public override object OutputDescription {
            get { return outputDescription; }
            set { outputDescription = value; }   }
    }

    public class Directed_Graph {
        public IList<Executable> Nodes { get; set; }
        public IDictionary<Executable, Executable> Edges { get; set; }

        public Directed_Graph() {
            Nodes = new List<Executable>();
            Edges = new Dictionary<Executable, Executable>();   }
    }
\end{verbatim}
\caption{The signatures of \emph{Executable}, \emph{Process}, and \emph{Directed Graph} classes}
\label{fig_classes}
\end{figure}


The \emph{Process} class, like the other executables, has required input, and possible output. It also contains a directed graph that describes the possible flow between different states (nodes) of the graph. As we mentioned previously, a node can be either a call to a Service, another Process, or some application logic (user selection, user input). In the \emph{Directed Graph} class, two nodes can be connected via an edge only if all the required input arguments of a node have been satisfied by some of the prior nodes. The signatures of \emph{Executable}, \emph{Process}, and \emph{Directed Graph} classes
are shown in Figure~\ref{fig_classes}.

For example, the \emph{Process} object corresponding to the work-flow diagram for examination creation shown in Figure~\ref{fig_workflow_exam_creation} will
have as an output either an electronic examination stored in the central database or an error if something went wrong.
The corresponding \emph{Directed Graph} will show the sequential order of executables, and how the input of each executable depends on
the outputs of executables that appear prior it in the execution order.


\subsection{Formal Verification of Processes}

The model enables us to verify whether the restrictions of the process are satisfied. It can be checked whether all the nodes in the graph can be visited and thus it can be determined whether the process contains unnecessary steps, or whether some steps (i.e.\ the input arguments) may be missing. It can also be verified whether certain data or entities that are returned as a response to certain services, are unused.
Figure \ref{fig_abstract_process_model} shows the abstract model. In the implementation of this model for the process of referring (creating an examination) patients, as a process, we may want to verify whether there is a free time-slot.

By defining pre- and post-conditions for input and output arguments, respectively, of each executable,
we can also formally verify that the whole process is correct.
The pre-conditions of input arguments describe for which inputs we will obtain successful output of the
given executable without causing any error.
The post-condition of output arguments describe possible outputs of an executable, such that the executables that
sequentially follow the given one and whose inputs depend on its output will fulfill their corresponding pre-conditions.

%\subsection{Formal Description of Entities}
%\subsection{On the Formal Verification of Entities}

\section{Conclusion}

In this paper, we have described the implementation of an enterprise e-health platform, where all medical and health related data are stored
in a centralized database and processed on-line. The platform allows integration with various health organizations, which can communicate with the central database in two directions (read and write). We have shown how the
architecture of the system can be transferred from process oriented to more formal one based on Model-Driven Architecture.
Finally, we describe the benefits that this transformation brings to the system, such as low complexity, standardization, interoperability, extensibility.

\bibliographystyle{tfcad}
\bibliography{paper}


\end{document}
